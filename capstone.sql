-- Return the customerName of the customers who are from the Philippines

SELECT customerName FROM customers WHERE country = "Philippines";

-- Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- Return the product name and MSRP of the product named "The Titanic"

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- Return the names of customers who have no registered state

SELECT customerName FROM customers WHERE state IS NULL;

-- Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

SELECT customerName, country, creditLimit from customers WHERE NOT country = "USA" AND creditLimit > 3000;

-- Return the customer numbers of orders whose comments contain the string 'DHL

SELECT orderNumber FROM orders WHERE comments LIKE "%DHL%";

-- Return the product lines whose text description mentions the phrase 'state of the art'

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- Return the countries of customers without duplication

SELECT DISTINCT country FROM customers; 

-- Return the statuses of orders without duplication

SELECT DISTINCT status FROM orders;

-- Return the customer names and countries of customers whose country is USA, France, or Canada

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- Return the first name, last name, and office's city of employees whose offices are in Tokyo

SELECT firstName, lastName, city FROM employees
    JOIN offices ON offices.officeCode = employees.officeCode
    WHERE city = "Tokyo";

-- Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT customerName FROM customers
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
    WHERE firstName = "Leslie" AND lastName = "Thompson";

-- Return the product names and customer name of products ordered by "Baane Mini Imports"

SELECT productName, customerName FROM products
    JOIN orderdetails ON products.productCode = orderdetails.productCode
    JOIN orders ON orderdetails.orderNumber = orders.orderNumber
    JOIN customers ON orders.customerNumber = customers.customerNumber
    WHERE customerName = "Baane Mini Imports";

-- Return the employee's first names, employee's last names, customer's names, and offices' countries of transaction whose customers and offices are in the same country

SELECT firstName, lastName, customerName, offices.country FROM employees
    JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
    JOIN offices ON employees.officeCode = offices.officeCode
    WHERE offices.country = customers.country;

-- Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantites less than 1000

SELECT productName, quantityInStock FROM productlines
    JOIN products ON productlines.productLine = products.productLine
    WHERE quantityInStock <1000 AND productlines.productLine = "planes";

-- Return the customer's name with a phone number containing "+81"

SELECT customerName, phone FROM customers WHERE phone LIKE "+81%";